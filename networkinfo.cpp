#include "networkinfo.h"

NetworkInfo::NetworkInfo(QWidget *parent) : QWidget(parent),
    m_layout(nullptr),
    m_PhoneNumber(nullptr),
    m_CarrierName(nullptr)
{
    m_layout = new QHBoxLayout();
    m_PhoneNumber = new QLabel("Phone Number");
    m_CarrierName = new QLabel("Carrier Name");
    m_layout->addWidget(m_PhoneNumber);
    m_layout->addWidget(m_CarrierName);
    this->setLayout(m_layout);
}

void NetworkInfo::SetCarrierName(QString CarrierName)
{
    m_CarrierName->setText(CarrierName);
}

void NetworkInfo::SetPhoneNumber(QString PhoneNumber)
{
    m_PhoneNumber->setText(PhoneNumber);
}

