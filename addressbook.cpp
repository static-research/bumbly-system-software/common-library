#include "addressbook.h"
/*
 *  QVBoxLayout * m_layout;
 *  QLineEdit * m_Search;
 *  QToolButton * m_Contacts;
 *  QSqlDatabase * m_DataBase;
 */
AddressBook::AddressBook() :
    m_layout(nullptr),
    m_Search(nullptr),
    m_Contacts(nullptr)
{
    RefreshAddressBook();
}

AddressBook::~AddressBook()
{
    delete m_layout;
    delete m_Search;
}

void AddressBook::AddNewContact()
{
    AddAContact * ContactWindow = new AddAContact;
    ContactWindow->setWindowModality(Qt::ApplicationModal);
//    connect(&m_MessageThreads[0], &QToolButton::clicked, this, &TextMessageApp::StartAddressBook);
    connect(ContactWindow, &AddAContact::CreatedContact, this, &AddressBook::RefreshAddressBook);
    ContactWindow->show();
}

void AddressBook::AcceptContact()
{
    QToolButton * SendingButton = static_cast<QToolButton *>(sender());
    //SendingButton->
    //QMessageBox::information(this, "Message", SendingButton->property("c_id").toString());
    //QMessageBox::information(this, "Message");
    //SendingButton->property("c_id").toString().toUtf8().data());
    emit ContactAccepted(SendingButton->property("c_id").toString());
    //this->close();

}

bool AddressBook::eventFilter(QObject * obj, QEvent *event)
{
    if (obj == this->m_Search)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent * keyEvent = static_cast<QKeyEvent *>(event);
            if(keyEvent->key() == Qt::Key_Down)
            {
                m_Contacts[0].setFocus();
                return true;
            }
        }
    }
    else {
        return QObject::eventFilter(obj, event);
    }
    return false;
}

void AddressBook::RefreshAddressBook()
{
    int size = -1;
    QSqlQuery Query;
    Query.prepare("SELECT * FROM Contacts");
    Query.exec();
    PurgeAddressBook();
    m_layout = new QVBoxLayout;
    m_Search = new QLineEdit;
    m_Contacts = new QToolButton[Query.size() + 1];
    m_Contacts[0].setText("Add New Contact");
    m_layout->addWidget(&m_Contacts[0]);
//    connect(&m_MessageThreads[0], &QToolButton::clicked, this, &TextMessageApp::StartAddressBook);
    connect(&m_Contacts[0], &QToolButton::clicked, this, &AddressBook::AddNewContact);
    m_Contacts[0].setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    size = Query.size();
    for (int idx = 1;idx < size + 1; ++idx)
    {
        Query.next();
        if (Query.value("c_id") != 0)
        {
            if (Query.isValid() && !Query.isNull("name"))
            {
                //std::string temp = Query.record().value("name").toString().toUtf8().constData();
                m_Contacts[idx].setText(Query.record().value("name").toString()
                                        + "\n" +
                                        Query.record().value("phonenumber").toString());
                m_Contacts[idx].setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
                m_Contacts[idx].setProperty("c_id", Query.record().value("c_id").toString());
                m_layout->addWidget(&m_Contacts[idx]);
                connect(&m_Contacts[idx], &QToolButton::clicked, this, &AddressBook::AcceptContact);
            }
        }
    }
    this->setLayout(m_layout);
    this->update();
}

void AddressBook::PurgeAddressBook()
{
    if (m_layout != nullptr)
    {
        delete m_layout;
        m_layout = nullptr;
    }
    if(m_Contacts != nullptr)
    {
        delete[] m_Contacts;
        m_Contacts = nullptr;
    }
    if (m_Search != nullptr)
    {
        delete m_Search;
        m_Search = nullptr;
    }
}
