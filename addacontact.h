#ifndef ADDACONTACT_H
#define ADDACONTACT_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QGridLayout>
#include <QToolButton>
#include <QVBoxLayout>

#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QMessageBox>

class AddAContact : public QWidget
{
    Q_OBJECT
public:
    AddAContact();

public slots:
    void AcceptContact();

signals:
    void CreatedContact();


private:
    QGridLayout * m_InformationLayout;
    QVBoxLayout * m_Layout;
    QLabel * m_NameLabel;
    QLineEdit * m_Name;
    QLabel * m_PhoneNumberLabel;
    QLineEdit * m_PhoneNumber;
    QLabel * m_NotesLabel;
    QTextEdit * m_Notes;
    QToolButton * m_Accept;
};

#endif // ADDACONTACT_H
