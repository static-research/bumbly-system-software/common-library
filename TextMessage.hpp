#pragma once
#include <string>
#include <stdio.h>
#include <QWidget>

using std::string;
class TextMessage
{
public:
    TextMessage(char * header, char * message);
    QString GetID();
    QString GetSource();
    QString GetMessage();
private:
    QString m_ID;
    QString m_Source;
    QString m_Message;
};
