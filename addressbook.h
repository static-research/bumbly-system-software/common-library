#ifndef ADDRESSBOOK_H
#define ADDRESSBOOK_H

#include <QWidget>
#include <QVBoxLayout>
#include <QToolButton>
#include <QLineEdit>
#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QMessageBox>
#include <QKeyEvent>

#include "addacontact.h"
class AddressBook : public QWidget
{
    Q_OBJECT
public:
    AddressBook();
    ~AddressBook();

public slots:
    void AddNewContact();
    void AcceptContact();

signals:
    void ContactAccepted(QString AcceptedContact);

protected:
    bool eventFilter(QObject * obj, QEvent * event);
private:
    void RefreshAddressBook();
    void PurgeAddressBook();
    QVBoxLayout * m_layout;
    QLineEdit * m_Search;
    QToolButton * m_Contacts;
    //QSqlDatabase * m_DataBase;
};

#endif // ADDRESSBOOK_H
