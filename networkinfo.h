#ifndef NETWORKINFO_H
#define NETWORKINFO_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
class NetworkInfo : public QWidget
{
    Q_OBJECT
public:
    explicit NetworkInfo(QWidget *parent = nullptr);
    void SetCarrierName(QString CarrierName);
    void SetPhoneNumber(QString PhoneNumber);
signals:

public slots:

private:
    QLabel * m_PhoneNumber;
    QLabel * m_CarrierName;
    QLayout * m_layout;
};

#endif // NETWORKINFO_H
