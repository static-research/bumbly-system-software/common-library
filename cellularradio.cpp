#include "cellularradio.h"
CellularRadio::CellularRadio(QString SerialPort, QString BaudRate):
    m_BaudRate(BaudRate),
    m_RadioPolling(nullptr)
{
    ssize_t NumberOfBytesWritten = -1;
    m_SerialPort = OpenPort(SerialPort);
    tcgetattr(m_SerialPort, &m_specs);
    m_specs.c_cflag = (CLOCAL | CREAD);
    m_specs.c_oflag = (OPOST | CR3);
    cfsetspeed(&m_specs, B115200);
    tcsetattr(m_SerialPort, TCSANOW, &m_specs);
    int flags = fcntl(m_SerialPort, F_GETFL, 0);
    fcntl(m_SerialPort, F_SETFL, flags | O_NONBLOCK);
    NumberOfBytesWritten = write(m_SerialPort, "ATE0\r\n", 100);
    if (NumberOfBytesWritten < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Disabling Echo Mode on Cellular Module");
    }
    try
    {
        DumpUntilOK();
    }
    catch (QString error)
    {
        QMessageBox::information(nullptr, "ERROR", error);
    }
    NumberOfBytesWritten = write(m_SerialPort, "AT+CMGF=1\r\n", 100);
    if (NumberOfBytesWritten < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Enabling Text Mode on Cellular Module");
    }
    try
    {
        DumpUntilOK();
    }
    catch (QString error)
    {
        QMessageBox::information(nullptr, "ERROR", error);
    }
    NumberOfBytesWritten = read(m_SerialPort, nullptr, 1000);

}

void CellularRadio::SendTextMessage(QString Destination, QString Message)
{
    ssize_t idx = 1;
    char Buffer[4096];
    //char * TX = "AT+CMGSEX=" + Destination + ""
    QString CommandString = "AT+CMGS=\"" + Destination + "\"\r";
    //Message += "\032";
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, CommandString.toStdString().c_str(), 100);
    if (idx < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Addressing TextMessage");
    }
    while(1)
    {
        idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
        if(idx >= 0)
        {
            //DumpUntilOK();
            break;
        }
    }
    Message += static_cast<char>(26);
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, Message.toStdString().c_str(), 100);
    if (idx < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Addressing TextMessage");
    }
    while(1)
    {
        idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
        if(idx >= 0)
        {
            //DumpUntilOK();
            break;
        }
    }
}

void CellularRadio::PlaceCall(QString Destination)
{
    ssize_t idx = 1;
    char Buffer[4096];
    QString CommandString = "ATD" + Destination + ";\r\n";
    //Message += "\032";
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, CommandString.toStdString().c_str(), 100);
    if (idx < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Addressing TextMessage");
    }
    while(1)
    {
        idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
        if(idx >= 0)
        {
            //DumpUntilOK();
            break;
        }
    }
}

void CellularRadio::EndCall()
{
    ssize_t idx = 1;
    char Buffer[4096];
    QString CommandString = "AT+CHUP\r\n";
    //Message += "\032";
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, CommandString.toStdString().c_str(), 100);
    if (idx < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Addressing TextMessage");
    }
    while(1)
    {
        idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
        if(idx >= 0)
        {
            //DumpUntilOK();
            break;
        }
    }
}

void CellularRadio::run()
{
    printf("Thread is Running\n");
    m_TimeThread = new QThread(this);
    m_RadioPolling = new QTimer(nullptr);
    connect(m_RadioPolling, &QTimer::timeout, this, &CellularRadio::PollRadio);
    m_RadioPolling->setInterval(250);
    m_RadioPolling->moveToThread(m_TimeThread);
    m_TimeThread->start();
}

QString CellularRadio::GetCarrier()
{
    ssize_t idx = 1;
    char Buffer[4096];
    char * temp = nullptr;
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, "AT+COPS=3,0\r\n", 100);
    if (idx < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Writing Carrier Command");
    }
    try
    {
        DumpUntilOK();
    }
    catch (QString error)
    {
        QMessageBox::information(nullptr, "ERROR", error);
    }
    idx = write(m_SerialPort, "AT+COPS?\r\n", 100);
    if (idx < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Writing Carrier Command");
    }
    while(1)
    {
        idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
        if(idx < 0)
        {
            if ((temp = strstr(Buffer, "+COPS: ")) != nullptr)
            {
                temp += 7;
                if (temp[0] == '2')
                {
                    strcpy(Buffer, "Emergency Calls Only");
                    temp = Buffer;
                    QMessageBox::information(nullptr, "Information", "Attempting to register SIM Card returned \"force deregister\" this usually means that this SIM Card is no longer active and can only be used for emergency calls.");
                }
                else
                {
                    /*Find Opening Quote*/
                    while(*temp !='\"')
                    {
                        temp++;
                    }
                    temp++;
                    /*Find Closing Quote*/
                    for (int idx = 1; temp[idx] != '\0'; ++idx)
                    {
                        if (temp[idx] == '\"')
                        {
                            temp[idx] = '\0';
                        }
                    }
                }
                break;
            }
            else if (strstr(Buffer, "ERROR") != nullptr)
            {
                return QString("ERROR in SIM");
            }
        }
    }
    return QString(temp);
}

QString CellularRadio::GetNumber()
{
    ssize_t idx = 1;
    char Buffer[4096];
    char * temp = nullptr;
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, "AT+CNUM\r\n", 100);
    if (idx < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Writing Carrier Command");
    }
    while(1)
    {
        idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
        if(idx < 0)
        {
            if ((temp = strstr(Buffer, "+CNUM: ")) != nullptr)
            {
                /*Advance to comma, where quote is*/
                while(*temp != ',')
                {
                    ++temp;
                }
                temp += 2;
                /*Find ending quote*/
                for (int idx = 1; temp[idx] != '\0'; ++idx)
                {
                    if (temp[idx] == '\"')
                    {
                        temp[idx] = '\0';
                    }
                }
                break;
            }
            else if (strstr(Buffer, "ERROR") != nullptr)
            {
                return QString("ERROR in SIM");
            }
        }
    }

    return QString(temp);
}

void CellularRadio::answerCall()
{
    ssize_t idx = 1;
    char Buffer[4096];
    QString CommandString = "ATA\r\n";
    //Message += "\032";
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, CommandString.toStdString().c_str(), 100);
    if (idx < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Addressing TextMessage");
    }
    while(1)
    {
        idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
        if(idx >= 0)
        {
            //DumpUntilOK();
            break;
        }
    }
}
void CellularRadio::CheckForMessages()
{
    ssize_t idx = 0;
    char Buffer[4096];
    char * Header = nullptr;
    char * Message = nullptr;
    memset(Buffer, 0, 4096);
    printf("We're checking for messages\r\n");
    /*AT+CMGL="REC UNREAD"*/
    /*"AT+CMGL=\"ALL\"\r\n"*/
    idx = write(m_SerialPort, "AT+CMGL=\"REC UNREAD\"\r\n", 200);
    usleep(250);
    idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
    if(idx > 0)
    {
        if (strstr(Buffer, "+CMGL:") != nullptr)
        {
            Header = strstr(Buffer, "+CMGL");
            Header = strtok(Header, "\r\n");
        }
        while(Header != nullptr)
        {
            if (strstr(Header, "+CMGL") != nullptr)
            {
                //Parse Message and header
                Message = strtok(nullptr, "\r\n");
                TextMessage Mess(Header, Message);
                //void TextMessageReceived(TextMessage message);
                TextMessageReceived(Mess);
                Header = strtok(nullptr, "\r\n");
                DeleteMessageFromModule(Mess.GetID().toInt());
            }
            else
            {
                break;
            }
        }
    }
}

void CellularRadio::DeleteMessageFromModule(int message)
{
    ssize_t idx = 0;
    char Buffer[4096];
    memset(Buffer, 0, 4096);
    sprintf(Buffer, "AT+CMGD=%d\r\n", message);
    /*AT+CMGL="REC UNREAD"*/
    idx = write(m_SerialPort, Buffer, 200);
    DumpUntilOK();
}

void CellularRadio::AnswerCall()
{
    ssize_t idx = 1;
    char Buffer[4096];
    QString CommandString = "ATA\r\n";
    //Message += "\032";
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, CommandString.toStdString().c_str(), 100);
    if (idx < 0)
    {
        QMessageBox::information(nullptr, "ERROR", "ERROR: Addressing TextMessage");
    }
    while(1)
    {
        idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
        if(idx >= 0)
        {
            //DumpUntilOK();
            break;
        }
    }
}

void CellularRadio::PollRadio()
{
    printf("Polling Radio\n");
    QMessageBox::information(nullptr, "Checking", "Checking For Messages");
    CheckForMessages();
}

bool CellularRadio::isRinging()
{   //AT+CPAS
    //Check for 3
    ssize_t idx = 0;
    char Buffer[4096];
    char * Header = nullptr;
    char * Message = nullptr;
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, "AT+CPAS\r\n", 200);
    usleep(250);
    idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
    if(idx > 0)
    {
        if (strstr(Buffer, "3") != nullptr)
        {
            return true;
        }
    }
    return false;
}

bool CellularRadio::isOnCall()
{   //AT+CPAS
    //Check for 4
    ssize_t idx = 0;
    char Buffer[4096];
    char * Header = nullptr;
    char * Message = nullptr;
    memset(Buffer, 0, 4096);
    idx = write(m_SerialPort, "AT+CPAS\r\n", 200);
    usleep(250);
    idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
    if(idx > 0)
    {
        if (strstr(Buffer, "4") != nullptr)
        {
            return true;
        }
    }
    return false;
}

void CellularRadio::DumpUntilOK()
{
    ssize_t idx;
    char Buffer[4096];
    memset(Buffer, 0, 4096);
    while(1)
    {
        idx = read(m_SerialPort, reinterpret_cast<void *>(Buffer), 4095);
        if(idx < 0)
        {
            if (strstr(Buffer, "OK") != nullptr)
            {
                break;
            }
            else if (strstr(Buffer, "ERROR") != nullptr)
            {
                throw QString(Buffer);
            }
        }
    }
}

int CellularRadio::OpenPort(QString Port)
{
    int port;
    port = open(Port.toStdString().c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    if(port != -1)
    {
        fcntl(port, F_SETFL, 0);
    }
    else
    {
        throw(std::string("ACK"));
    }
    return port;
}
