#include "addacontact.h"

AddAContact::AddAContact()
{
    m_InformationLayout = new QGridLayout;
    m_Layout = new QVBoxLayout;
    m_PhoneNumberLabel = new QLabel;
    m_PhoneNumberLabel->setText("Phone Number");
    m_PhoneNumber = new QLineEdit;
    m_NameLabel = new QLabel;
    m_NameLabel->setText("Name");
    m_Name = new QLineEdit;
    m_Notes = new QTextEdit;
    m_Accept = new QToolButton;
    m_Accept->setText("Accept");
    m_InformationLayout->addWidget(m_NameLabel, 0, 0);
    m_InformationLayout->addWidget(m_Name, 0, 1);
    m_InformationLayout->addWidget(m_PhoneNumberLabel, 1, 0);
    m_InformationLayout->addWidget(m_PhoneNumber, 1, 1);
    m_Layout->addLayout(m_InformationLayout);
    m_NotesLabel = new QLabel;
    m_NotesLabel->setText("Notes:");
    m_Layout->addWidget(m_NotesLabel);
    m_Layout->addWidget(m_Notes);
    m_Layout->addWidget(m_Accept);
    m_Accept->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    this->setLayout(m_Layout);

    connect(m_Accept, &QToolButton::clicked, this, &AddAContact::AcceptContact);
}

void AddAContact::AcceptContact()
{
    QSqlQuery Query;
    Query.prepare("INSERT INTO Contacts(name, notes, phonenumber)"
                  "VALUES(:name,:notes,:phonenumber)");
    Query.bindValue(":name", m_Name->text());
    Query.bindValue(":notes", m_Notes->toPlainText());
    Query.bindValue(":phonenumber", m_PhoneNumber->text());
    if(Query.exec())
    {
        emit CreatedContact();
    }
    this->close();
}
