#ifndef CELLULARRADIO_H
#define CELLULARRADIO_H
#include <QThread>
#include <QTimer>
#include <string.h>
#include <stdlib.h>
#include <QMessageBox>
#include <termios.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include "TextMessage.hpp"
class CellularRadio : public QThread
{
    Q_OBJECT
public:
    CellularRadio(QString SerialPort, QString BaudRate);
    void SendTextMessage(QString Destination, QString Message);
    void PlaceCall(QString Destination);
    void EndCall();
    void run() override;
    QString GetCarrier();
    QString GetNumber();
    bool isRinging();
    bool isOnCall();
    void answerCall();
    void CheckForMessages();
    void DeleteMessageFromModule(int message);
    void AnswerCall();
signals:
    void TextMessageReceived(TextMessage message);
    void CallEnded();
    void CallReceived(QString Source);
    void VoiceMailReceived();

private slots:
    void PollRadio();


private:
    void PollTexts();
    void PollCalls();
    void DumpUntilOK();
    int m_SerialPort;
    QString m_BaudRate;
    QTimer * m_RadioPolling;
    static int OpenPort(QString Port);
    struct termios m_specs;
    QThread * m_TimeThread;
};

#endif // CELLULARRADIO_H
