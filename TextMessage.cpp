#include "TextMessage.hpp"
TextMessage::TextMessage(char *header, char *message)
{
    char * InternalHeader = strdup(header);
    char * temp = strtok(InternalHeader, ",");
    /*
    QString m_ID;
    QString m_Source;
    QString m_Message;
    */
    m_Message = QString(message);
    temp += 7;
    m_ID = QString(temp);
    temp = strtok(nullptr, ",");
    temp = strtok(nullptr, ",");
    temp += 3;
    temp[10] = '\0';
    m_Source = QString(temp);
    free(InternalHeader);
}

QString TextMessage::GetID()
{
    return m_ID;
}

QString TextMessage::GetSource()
{
    return m_Source;
}

QString TextMessage::GetMessage()
{
    return m_Message;
}
